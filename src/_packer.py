import os
import py_compile
import marshal
import pickle
import sys
import imp
from collections import OrderedDict

REJECT_LIMIT = 100

def _splitPath(path):
    '''splits a file or folder path and returns as a list
    'D:/path/to/folder/or/file' -> ['D:', 'path', 'to', 'folder', 'or', 'file']
    '''
    components = []
    while True:
        (path, tail) = os.path.split(path)
        if tail == "":
            if path:
                components.append(path)
            components.reverse()
            return components
        components.append(tail)

def _collectModuleFiles(path):
    modules = OrderedDict()
    for root, dirs, files in os.walk(path):
        for phile in files:
            _, ext = os.path.splitext(phile)
            ext = ext.lower()
            if ext == '.py':
                modules.update(_compileModuleFile(path,
                                os.path.join(root, phile)))
            else:
                pass
    return modules

def _compileModuleFile(path, filename):
    py_compile.compile(filename, doraise=True)
    with open(filename + 'c', 'rb') as pycfile:
        data = pycfile.read()[8:]
        rel_path = os.path.relpath(filename, path)
        rel_path, ext = os.path.splitext(rel_path)
        packages = _splitPath(rel_path)
        isPackage = False
        if packages[-1] == '__init__':
            packages = packages[:-1]
            isPackage = True
        packages.insert(0, os.path.basename(path))
        return {'.'.join(packages): (data, isPackage)}

def generateInstallerData(path, dataPath):
    modules = _collectModuleFiles(path)
    data = pickle.dumps(modules)
    with open(dataPath, 'wb') as datafile:
        datafile.write(data)

def installData(datafile):
    data = None
    resource_root = os.path.dirname(datafile)
    missing_packages = []
    with open(datafile, 'rb') as datafile1:
        data = datafile1.read()
        data = pickle.loads(data)
    if data:
        reject_count = {}
        modulenames = data.keys()

        for mn in modulenames:
            sys.modules[mn] = imp.new_module(mn)

        while modulenames:

            modulename = modulenames.pop(0)
            binaryData, isPackage = data[modulename]

            sys.modules[modulename] = imp.new_module(modulename)
            main_package = modulename.split('.')
            if isPackage:
                sys.modules[modulename].__path__ = os.path.join(
                        resource_root, *main_package)
                main_package.append('__init__')
            sys.modules[modulename].__file__ = os.path.join(
                    resource_root, *main_package)+'.pyc'
            code = marshal.loads(binaryData)
            try:
                exec code in sys.modules[modulename].__dict__
            except (ImportError, AttributeError) as e:
                rc = reject_count.get(modulename, 0)
                if rc < REJECT_LIMIT:
                    reject_count[modulename] = rc + 1
                    modulenames.append(modulename)
                else:
                    print 'RC limit exceeded for %s' % modulename
                    import traceback
                    traceback.print_exc()
                    del sys.modules[modulename]
            except BaseException as e:
                print modulename, 'IGNORED', str(e)
                import traceback
                traceback.print_exc()
            else:
                print 'DONE'
