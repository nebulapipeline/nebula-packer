from setuptools import setup
from Cython.Build import cythonize

setup(name='packer', ext_modules=cythonize('packer/src/_packer.py'))
